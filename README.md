# Pack N Crypt
tar, gzip and gpg encrypt a directory in preperation of internetwork traversal. 

---
## What does this dogshit even do?
Wow, I'm not a fan of your tone, but I guess I'll explain... You provide a source directory (what you want to back up), and then you provide an output destination (where the packaged up and encrypted file will export to). The script handles the more tedious parts (I'm looking at you, GnuPG) automatically. You're left with a hot and steamy .tgz.gpg file that can be moved to your heart's content. **Just don't lose the key that gets generated...** (replace the key variable with something else if needed).

---
## How do I use this terrible looking script?
First things first, double-check the script and make sure it looks good to you. Nothing worse than getting your system sabotaged by an internet stranger's shitty shell script. Once you feel comforable with the mess you just read through:

Usage:
```
./backup.sh [source/] [destination]
```

Example (as demonstrated on 2023-02-10):
```
$ ./backup.sh /var/log/ /tmp/logbackup
===[ KEY ]======================================================
Key: feb1b2f2210523785d164a023a24ced4
===[ CER ]======================================================
-----BEGIN PGP PUBLIC KEY BLOCK-----
(public key information here)
-----END PGP PUBLIC KEY BLOCK-----
===[ TGZ ]======================================================
 802MiB 0:01:01 [13.1MiB/s] [                       <=>        ]
===[ GPG ]======================================================
 802MiB 0:00:01 [ 409MiB/s] [ <=>                              ]
Your encrypted file is ready: '/tmp/logbackup_20230210.tgz.gpg'
===[ END ]======================================================
```

Make sure not to append anything to the end of your destination. The script will automatically append the date and .tgz.gpg based off of your input. 

Decrypt and check:  
```
$ gpg --batch --passphrase "[KEY]" --decrypt logbackup_20230210.tgz.gpg > logbackup_20230210.tgz`
gpg: AES256.CFB encrypted data
gpg: encrypted with 1 passphrase
$ tar -tzf logbackup_20230210.tgz
list...
of...
files...
here...
...
```
---
## Essential Dependencies
aka, thank you GNU for your wonderful coreutils and such
- [bash](https://www.gnu.org/software/bash/manual/html_node/)
- [date](https://www.gnu.org/software/coreutils/manual/html_node/date-invocation.html)
- [echo](https://www.gnu.org/software/coreutils/manual/html_node/echo-invocation.html)
- [gnupg](https://gnupg.org/documentation/manuals/gnupg/)
- [md5sum](https://www.gnu.org/software/coreutils/manual/html_node/md5sum-invocation.html)
- [pv](https://www.ivarch.com/programs/pv.shtml)
- [rm](https://www.gnu.org/software/coreutils/manual/html_node/rm-invocation.html)
- [tar](https://www.gnu.org/software/tar/manual/html_node/)
- [whoami](https://www.gnu.org/software/coreutils/manual/html_node/whoami-invocation.html)

---
## Exit Codes
- 0: Success! You have an encrypted file!
- 1: Incorrect amount of paramaters/arguments provided. Help information displayed.
- 2: Source directory doesn't exist.
- 3: Destination .gpg file already exists. Avoiding accidental overwrite.

---
## Why?
Why not? I don't know honestly. This isn't anything new, just writing it after troubleshooting a script a friend was constructing. Ended up developing it into something a bit... *more*.

---
## Final Note
Shell scripts out for Harambe.