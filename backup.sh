#!/bin/bash
date=`date +%Y%m%d`

help(){
    echo "===[ HELP ]====================================================="
    echo "Usage: ./backup.sh [source/] [destination]"
    echo "---"
    echo "Example: ./backup.sh /var/log/ /tmp/logbackup"
    echo "Output File: /tmp/logbackup_$date.tgz.gpg"
    echo "================================================================"
}

#check for commandline input. exit if nothing.
if [ -z "$1" ] || [ -z "$2" ]; then
  help
  exit 1
fi

inputDir="$1"
#check to see if the source directory is real. exit if not.
if [ ! -d "$inputDir" ]; then
  echo "The directory '$inputDir' could not be found."
  exit 2
fi

outputCompressed="$2_$date.tgz"
outputEncrypted="$outputCompressed.gpg"
#check to see if the output gpg file already exists. exit if it does.
if [ -a "$outputCompressed" ]; then
  echo "The file '$outputEncrypted' already exists. Exiting to avoid accidental overwrite."
  exit 3
fi

#generate new gnupg id (or resue existing of same name)
gpgID="$(whoami)_remote"
key=`echo $gpgID | md5sum | cut -d' ' -f1`
echo "===[ KEY ]======================================================"
echo "Key: $key"
echo "===[ CER ]======================================================"
gpg --quiet --batch --passphrase "$key" --quick-generate-key "$gpgID" rsa4096 encr never
gpg --armor --export $gpgID

#compress and pack tarball. show progress.
echo "===[ TGZ ]======================================================"
tar czPf - "$inputDir" | (pv --progress --timer --rate --bytes --width 64 > "$outputCompressed")

#encrypt compressed tarball. use show progress.
echo "===[ GPG ]======================================================"
gpg --batch --default-key "$gpgID" --cipher-algo AES256 --passphrase "$key" -co - "$outputCompressed" | (pv --progress --timer --rate --bytes --width 64 > "$outputEncrypted")

#cleanup
rm "$outputCompressed"
echo "Your encrypted file is ready: '$outputEncrypted'"
echo "===[ END ]======================================================"